<?php

     include 'config.inc.php';

	 // Check whether bookingRef or password is set from android
     if(isset($_POST['bookingRef']) && isset($_POST['pWord']))
     {
		  // Innitialize Variable
		  $result='';
		  $bookingRef = '';
          $pWord = '';
	   	  $bookingRef = $_POST['bookingRef'];
          $pWord = $_POST['pWord'];

		  // Query database for row exist or not
          $sql = 'SELECT * FROM users WHERE  bookingRef = :bookingRef AND pWord = :pWord';
          $stmt = $conn->prepare($sql);
          $stmt->bindParam(':bookingRef', $bookingRef, PDO::PARAM_STR);
          $stmt->bindParam(':pWord', $pWord, PDO::PARAM_STR);
          $stmt->execute();
          if($stmt->rowCount())
          {
			 $result="true";
          }
          elseif(!$stmt->rowCount())
          {
			  	$result="false";
          }

		  // send result back to android
   		  echo $result;
  	}

?>