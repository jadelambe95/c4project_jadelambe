package com.example.jadel.c4project_jadelambe;

/**
 * Created by JadeL on 09/02/2017.
 */

public class User {
    public static final String TAG = User.class.getSimpleName();
    public static final String TABLE = "Users";

    // Labels Table Columns names
    public static final String KEY_BookingRef = "BookingRef";
    public static final String KEY_Name = "Name";
    public static final String KEY_Address = "Address";
    public static final String KEY_CCard = "CreditCard";
    public static final String KEY_PWord = "PassWord";

    private String bookingRef ;
    private String name;
    private String address ;
    private int ccard;
    private String pword ;

    public String getBookingRef() {
        return bookingRef;
    }
    public void setBookingRef(String bookingRef) {
        this.bookingRef = bookingRef;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCCard() {
        return ccard;
    }

    public void setCCard(int ccard) {
        this.ccard = ccard;
    }

    public String getPWord() {
        return pword;
    }

    public void setPWord(String pword) {
        this.pword = pword;
    }
}