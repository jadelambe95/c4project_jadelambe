package com.example.jadel.c4project_jadelambe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


/**
 * Created by JadeL on 16/02/2017.
 */

public class HomeActivity extends Activity implements View.OnClickListener {

    String passedVar=null;
    private TextView passedView=null;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Button btn1 = (Button) findViewById(R.id.buttonMyKids);
        btn1.setOnClickListener(this);

        Button btn2 = (Button) findViewById(R.id.buttonFindPalz);
        btn2.setOnClickListener(this);

        Button btn3 = (Button) findViewById(R.id.buttonMessages);
        btn3.setOnClickListener(this);

        //Get our passed variable from our intent's Extras
        passedVar=getIntent().getStringExtra(MainActivity.ID_EXTRA);
        //find out text view
        passedView=(TextView) findViewById(R.id.passed);
        //display our passed variable
        passedView.setText("");

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.buttonMyKids:
                startActivity(new Intent(HomeActivity.this, MyKids.class));
                break;

            case R.id.buttonFindPalz:
                startActivity(new Intent(HomeActivity.this, FindPalz.class));
                break;

            case R.id.buttonMessages:
                startActivity(new Intent(HomeActivity.this, Messages.class));
                break;


            default:
                break;
        }
    }
}
