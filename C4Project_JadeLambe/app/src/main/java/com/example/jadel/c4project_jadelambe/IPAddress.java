package com.example.jadel.c4project_jadelambe;

public class IPAddress {
    private static IPAddress mInstance= null;

    public String ip = "10.102.11.51";

    public String bookingRef = "";
    public int test;
    public int id = 0;

    protected IPAddress(){}

    public static synchronized IPAddress getInstance(){
        if(null == mInstance){
            mInstance = new IPAddress();
        }
        return mInstance;
    }
}