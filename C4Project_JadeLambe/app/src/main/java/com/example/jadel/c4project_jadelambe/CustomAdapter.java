package com.example.jadel.c4project_jadelambe;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class CustomAdapter extends ArrayAdapter<Kid> {



    int groupid;

    ArrayList<Kid> records;

    Context context;



    public CustomAdapter(Context context, int vg, int id, ArrayList<Kid>
            records) {

        super(context, vg, id, records);

        this.context = context;

        groupid = vg;

        this.records = records;



    }



    public View getView(int position, View convertView, ViewGroup parent) {



        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(groupid, parent, false);

        TextView textName = (TextView) itemView.findViewById(R.id.kid_name);

        textName.setText(records.get(position).getFName());

        TextView textPrice = (TextView) itemView.findViewById(R.id.kid_age);

        textPrice.setText(records.get(position).getAge());



        return itemView;

    }

}
