


package com.example.jadel.c4project_jadelambe;

import java.util.List;
import java.util.Set;

/**
 * Created by JadeL on 09/02/2017.
 */

public class Kid {
    public int ID;


    public static final String TAG = Kid.class.getSimpleName();
    public static final String TABLE = "Kids";
    // Labels Table Columns names
    public static final String KEY_BookingRef = "BookingRef";
    public static final String KEY_fName = "FirstName";
    public static final String KEY_Gender = "Gender"; //2 or more
    public static final String KEY_Age = "Age";
    public static final String KEY_Likes = "Likes";
    public static final String KEY_Languages = "Languages";
    public static final String KEY_Photo = "Photo";

    private String bookingRef;
    private String fName;
    private String gender;
    private String age;
    private Set<ActivityType> likes;
    private String languages;
    private String photo;


    public String getBookingRef() {
        return bookingRef;
    }

    public void setBookingRef(String bookingRef) {
        this.bookingRef = bookingRef;
    }

    public String getFName() {
        return fName;
    }

    public void setFName(String fName) {
        this.fName = fName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void addLike(ActivityType like) {
        this.likes.add(like);
    }

    public void removeLike(ActivityType like) {
        this.likes.remove(like);
    }

    public Set<ActivityType> getLikes()
    {
        return this.likes;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getID() {
        return ID;
    }
}

