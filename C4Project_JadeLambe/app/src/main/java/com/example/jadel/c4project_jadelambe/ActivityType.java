package com.example.jadel.c4project_jadelambe;

/**
 * Created by JadeL on 10/02/2017.
 */

public enum ActivityType {
    FOOTBALL, ARTS_AND_CRAFTS, GAMES, MUSIC_AND_DANCE, CARS, DOLLS;

}
