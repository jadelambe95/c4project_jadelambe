package com.example.jadel.c4project_jadelambe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.util.ArrayList;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.ListView;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import android.os.AsyncTask;

/**
 * Created by JadeL on 16/02/2017.
 */

public class MyKids extends Activity {

// Ref: Video on passing data from one file to another
// https://www.youtube.com/watch?v=XPKb_JqeTp8
    public final static String ID_EXTRA = "com.example.jadel.c4project_jadelambe._ID";


    Activity context;
    HttpPost httppost;
    HttpResponse response;
    HttpClient httpclient;
    ProgressDialog pd;
    CustomAdapter adapter;
    ListView listKids;
    ArrayList<Kid> records;


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mykids);

        context = this;

        records = new ArrayList<Kid>();

        listKids = (ListView) findViewById(R.id.kids_list);

        adapter = new CustomAdapter(context, R.layout.list_item, R.id.kid_name,
                records);

        listKids.setAdapter(adapter);

        listKids.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent i = new Intent(com.example.jadel.c4project_jadelambe.MyKids.this, ProfileActivity.class);
                IPAddress.getInstance().test = position + 1;
                i.putExtra(ID_EXTRA, String.valueOf(id));

                int childID = records.get(position).getID();
                i.putExtra(ID_EXTRA, childID);

                startActivity(i);




            }
        });

        Button btn = (Button) findViewById(R.id.buttonFPBack);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(com.example.jadel.c4project_jadelambe.MyKids.this, HomeActivity.class));
            }
        });

        Button btn2 = (Button) findViewById(R.id.buttonAdd);

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyKids.this, AddKid.class));
            }
        });


    }

    public void onStart() {

        super.onStart();

        //execute background task

        com.example.jadel.c4project_jadelambe.MyKids.BackTask bt = new com.example.jadel.c4project_jadelambe.MyKids.BackTask();
        bt.execute();
    }


    //background process to make a request to server and list product information
    private class BackTask extends AsyncTask<Void, Void, Void> {
        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(context);
            pd.setTitle("Retrieving data");
            pd.setMessage("Please wait.");
            pd.setCancelable(true);
            pd.setIndeterminate(true);
            pd.show();
        }

        protected Void doInBackground(Void... params) {

            InputStream is = null;

            String result = "";
            try {
                httpclient = new DefaultHttpClient();
                //use booking ref to find kids related to user
                String bookingRef = IPAddress.getInstance().bookingRef;
                httppost = new HttpPost("http://" + IPAddress.getInstance().ip + "/mykids_jade.php?bookingRef=" + "'" + bookingRef + "'");
                response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();

                // Get response as a String.
                is = entity.getContent();
            } catch (Exception e) {
                if (pd != null)
                    pd.dismiss(); //close the dialog if error occurs
                Log.e("ERROR", e.getMessage());
            }

            //convert response to string
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);

                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();

                result = sb.toString();

            } catch (Exception e) {
                Log.e("ERROR", "Error converting result " + e.toString());
            }


            //parse json data
            try {
                // Remove unexpected characters that might be added to beginning of the string
                result = result.substring(result.indexOf("["));
                JSONArray jArray = new JSONArray(result);

                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject json_data = jArray.getJSONObject(i);

                    Kid k = new Kid();

                    k.setFName(json_data.getString("name"));
                    k.setAge(json_data.getString("age"));
                    IPAddress.getInstance().test = json_data.getInt("id");

                    records.add(k);
                }
            } catch (Exception e) {
                Log.e("ERROR", "Error pasting data " + e.toString());
            }

            return null;
        }

        protected void onPostExecute(Void result) {

            if (pd != null) pd.dismiss(); //close dialog
            Log.e("size", records.size() + "");

            adapter.notifyDataSetChanged(); //notify the ListView to get new records
        }
    }

}