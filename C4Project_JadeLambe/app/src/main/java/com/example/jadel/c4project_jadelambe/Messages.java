package com.example.jadel.c4project_jadelambe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class Messages extends AppCompatActivity {
    private TextView textView ;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        textView = (TextView)findViewById(R.id.textView1);
        textView.setText("Welcome to Messages");

        Button btn1 = (Button) findViewById(R.id.buttonBack);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Messages.this, HomeActivity.class));
            }
        });

    }
}