package com.example.jadel.c4project_jadelambe;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;

public class ProfileAdapter extends BaseAdapter
{
    Context context;
    List<Profile> valueList;
    public ProfileAdapter(List<Profile> listValue, Context context)
    {
        this.context = context;
        this.valueList = listValue;
    }

    @Override
    public int getCount()
    {
        return this.valueList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return this.valueList.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewItem viewItem = null;
        if(convertView == null)
        {
            viewItem = new ViewItem();
            LayoutInflater layoutInflater = (LayoutInflater)this.context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.activity_adapter, null);

            viewItem.txtName = (TextView)convertView.findViewById(R.id.adapter_text_name);
            viewItem.txtDetails = (TextView)convertView.findViewById(R.id.adapter_text_details);
            viewItem.txtLanguages = (TextView)convertView.findViewById(R.id.adapter_text_languages);
            convertView.setTag(viewItem);
        }
        else
        {
            viewItem = (ViewItem) convertView.getTag();
        }

        viewItem.txtName.setText(valueList.get(position).name);
        viewItem.txtDetails.setText(valueList.get(position).age);
        viewItem.txtLanguages.setText(valueList.get(position).languages);

        return convertView;
    }

    class ViewItem
    {
        TextView txtName;
        TextView txtDetails;
        TextView txtLanguages;
    }

}

